#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/cred.h>
#include <linux/mm.h>
#include <asm/pgtable.h>

#include "char_device.h"

unsigned int buffer_size = PAGE_SIZE;
static unsigned int allow_mmap = 1;

module_param(buffer_size, uint, 0444);
module_param(allow_mmap, uint, 0444);

MODULE_LICENSE("Dual BSD/GPL");

static struct char_dev *char_device;
static dev_t dev;

int char_device_open(struct inode *inode, struct file *filp)
{
	struct char_dev *dev;

	dev = container_of(inode->i_cdev, struct char_dev, cdev);
	filp->private_data = dev;

	return 0;
}

int char_device_release(struct inode *inode, struct file *filp)
{
	return 0;
}

ssize_t char_device_read(struct file *filp, char __user *buf,
	size_t count, loff_t *f_pos)
{
	struct char_dev *dev = filp->private_data;

	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;

	/* No data available */
	while (dev->head == dev->tail) {
		mutex_unlock(&dev->lock);
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;
		if (wait_event_interruptible(dev->read_queue,
			(dev->head != dev->tail)))
			return -ERESTARTSYS;
		if (mutex_lock_interruptible(&dev->lock))
			return -ERESTARTSYS;
	}

	if (dev->tail < dev->head) /* No overlap */
		count = min(count, (size_t)(dev->head - dev->tail));
	else
		count = min(count, (size_t)(dev->end - dev->tail));

	if (copy_to_user(buf, dev->tail, count)) {
		mutex_unlock(&dev->lock);
		return -EFAULT;
	}

	dev->tail += count;
	if (dev->tail == dev->end)
		dev->tail = dev->buffer;

	/* Set pid and uid */
	dev->access_pid = current->pid;
	dev->access_uid = current_uid().val;

	mutex_unlock(&dev->lock);
	wake_up_interruptible(&dev->write_queue);

	return count;
}

static int spacefree(struct char_dev *dev)
{
	return dev->tail == dev->head ? dev->size - 1 :
		((dev->tail + dev->size - dev->head) % dev->size) - 1;
}

ssize_t char_device_write(struct file *filp, const char __user *buf,
	size_t count, loff_t *f_pos)
{
	struct char_dev *dev = filp->private_data;

	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;

	/* No space available */
	while (spacefree(dev) == 0) {
		mutex_unlock(&dev->lock);
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;
		if (wait_event_interruptible(dev->write_queue,
			(spacefree(dev) != 0)))
			return -ERESTARTSYS;
		if (mutex_lock_interruptible(&dev->lock))
			return -ERESTARTSYS;
	}

	count = min(count, (size_t)spacefree(dev));
	if (dev->head >= dev->tail)
		count = min(count, (size_t)(dev->end - dev->head));
	else
		count = min(count, (size_t)(dev->tail - dev->head - 1));

	if (copy_from_user(dev->head, buf, count)) {
		mutex_unlock(&dev->lock);
		return -EFAULT;
	}

	dev->head += count;
	if (dev->head == dev->end)
		dev->head = dev->buffer;

	/* Set pid and uid */
	dev->modify_pid = current->pid;
	dev->modify_uid = current_uid().val;

	mutex_unlock(&dev->lock);
	wake_up_interruptible(&dev->read_queue);

	return count;
}

long char_device_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct char_dev *dev = filp->private_data;
	struct dev_info info;
	int retval = 0;

	if ((_IOC_TYPE(cmd) != CHAR_DEVICE_IOC_MAGIC) ||
		(_IOC_NR(cmd) > CHAR_DEVICE_IOC_MAXNR))
		return -ENOTTY;

	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;

	switch (cmd) {
	case CHAR_DEVICE_IOCGLASTREAD:
		info.pid = dev->access_pid;
		info.uid = dev->access_uid;
		info.time = filp->f_path.dentry->d_inode->i_atime;
		/* TODO: user doesnt have timespec64 struct, how to send time? */
		if (copy_to_user((void __user *)arg, &info,
			sizeof(struct dev_info)))
			retval = -EFAULT;
		break;
	case CHAR_DEVICE_IOCGLASTWRITE:
		info.pid = dev->modify_pid;
		info.uid = dev->modify_uid;
		info.time = filp->f_path.dentry->d_inode->i_mtime;
		if (copy_to_user((void __user *)arg, &info,
			sizeof(struct dev_info)))
			retval = -EFAULT;
		break;
	default:
		retval = -ENOTTY;
	}

	mutex_unlock(&dev->lock);
	return retval;
}

static void char_device_vma_open(struct vm_area_struct *vma)
{

}

static void char_device_vma_close(struct vm_area_struct *vma)
{

}

static vm_fault_t char_device_vma_nopage(struct vm_fault *vmf)
{
	unsigned long offset;
	struct vm_area_struct *vma = vmf->vma;
	struct char_dev *dev = vma->vm_private_data;
	struct page *page;
	void *pageptr;
	vm_fault_t retval = VM_FAULT_NOPAGE;

	mutex_lock(&dev->lock);
	offset = (unsigned long)(vmf->address - vma->vm_start) +
		(vma->vm_pgoff << PAGE_SHIFT);
	if (offset >= dev->size)
		goto out;

	pageptr = dev->buffer + offset;
	if (!pageptr)
		goto out;
	page = virt_to_page(pageptr);

	get_page(page);
	vmf->page = page;

	mutex_unlock(&dev->lock);
	return 0;

out:
	mutex_unlock(&dev->lock);
	return retval;
}

static const struct vm_operations_struct char_device_vm_ops = {
	.open = char_device_vma_open,
	.close = char_device_vma_close,
	.fault = char_device_vma_nopage,
};

static int char_device_mmap(struct file *filp, struct vm_area_struct *vma)
{
	if (!allow_mmap)
		return -ENODEV;

	vma->vm_ops = &char_device_vm_ops;
	vma->vm_private_data = filp->private_data;
	char_device_vma_open(vma);

	return 0;
}

static const struct file_operations char_dev_fops = {
	.owner = THIS_MODULE,
	.read = char_device_read,
	.write = char_device_write,
	.unlocked_ioctl = char_device_ioctl,
	.mmap = char_device_mmap,
	.open = char_device_open,
	.release = char_device_release,
};

static void char_device_module_exit(void)
{
	if (char_device) {
		cdev_del(&char_device->cdev);
		if (allow_mmap)
			free_page((unsigned long)char_device->buffer);
		else
			kfree(char_device->buffer);
		kfree(char_device);
	}

	/* Unregister device */
	unregister_chrdev_region(dev, 1 /*count*/);
	pr_info("char_device module successfully exited\n");
}

static int char_device_module_init(void)
{
	int result;

	/* Get major and minor numbers for device */
	result = alloc_chrdev_region(&dev, 0 /*firstminor*/, 1 /*count*/,
		"char_device" /*name*/);
	if (result < 0) {
		pr_info("char_device: can't get major number\n");
		return result;
	}

	/* Create and initialize char device */
	char_device = kzalloc(sizeof(struct char_dev), GFP_KERNEL);
	if (!char_device) {
		result = -ENOMEM;
		pr_info("char_device: can't allocate memory for device\n");
		goto fail;
	}

	if (allow_mmap) {
		char_device->buffer = (void *)get_zeroed_page(GFP_KERNEL);
		char_device->size = PAGE_SIZE;
	} else {
		char_device->buffer = kcalloc(buffer_size, sizeof(char),
			GFP_KERNEL);
		char_device->size = buffer_size;
	}
	if (!char_device->buffer) {
		result = -ENOMEM;
		pr_info("char_device: can't allocate memory for device buffer\n");
		goto fail;
	}

	char_device->end = char_device->buffer + char_device->size;
	char_device->head = char_device->buffer;
	char_device->tail = char_device->buffer;
	mutex_init(&char_device->lock);
	init_waitqueue_head(&char_device->read_queue);
	init_waitqueue_head(&char_device->write_queue);
	cdev_init(&char_device->cdev, &char_dev_fops);
	char_device->cdev.owner = THIS_MODULE;

	result = cdev_add(&char_device->cdev, dev, 1 /*count*/);
	if (result < 0) {
		pr_info("char_device: error adding device: %d\n", result);
		goto fail;
	}

	pr_info("char_device module successfully initialized\n");
	return 0;

fail:
	char_device_module_exit();
	return result;
}

module_init(char_device_module_init);
module_exit(char_device_module_exit);
