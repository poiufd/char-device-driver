#ifndef _CHAR_DEVICE_H_
#define _CHAR_DEVICE_H_

#include <linux/ioctl.h>

struct char_dev {
	char *buffer;
	char *end;
	unsigned int size;
	char *head;
	char *tail;
	pid_t access_pid;
	uid_t access_uid;
	pid_t modify_pid;
	uid_t modify_uid;
	struct mutex lock;
	wait_queue_head_t read_queue, write_queue;
	struct cdev cdev;
};

extern unsigned int buffer_size;

int char_device_open(struct inode *inode, struct file *filp);
int char_device_release(struct inode *inode, struct file *filp);
ssize_t char_device_read(struct file *filp, char __user *buf, size_t count,
	loff_t *f_pos);
ssize_t char_device_write(struct file *filp, const char __user *buf,
	size_t count, loff_t *f_pos);
long char_device_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);

struct dev_info {
	pid_t pid;
	uid_t uid;
	struct timespec64 time;
};

#define CHAR_DEVICE_IOC_MAGIC       0x82 /* Not used one */

#define CHAR_DEVICE_IOCGLASTREAD    _IOR(CHAR_DEVICE_IOC_MAGIC, 0, struct dev_info)
#define CHAR_DEVICE_IOCGLASTWRITE   _IOR(CHAR_DEVICE_IOC_MAGIC, 1, struct dev_info)

#define CHAR_DEVICE_IOC_MAXNR 1

#endif /* _CHAR_DEVICE_H_ */
